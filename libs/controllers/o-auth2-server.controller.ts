import { Controller, HttpCode, HttpStatus, Post, Req, Res } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { Oauth2Service } from '../services/oauth2.service';

// https://github.com/oauthjs/express-oauth-server/blob/master/examples/postgresql/index.js

@ApiTags('OAuth2')
@Controller({
  path: 'oauth2',
  version: '1',
})
export class OAuth2ServerController {
  constructor(private readonly oauth2Service: Oauth2Service) {
  }

  /**
   * A Promise that resolves to the token object returned from Model#saveToken(). In case of an error, the promise rejects with one of the error types derived from OAuthError.
   */
  @ApiResponse({
    schema: {
      type: 'object',
      properties: {
        accessToken: {
          type: 'string',
        },
        accessTokenExpiresAt: {
          type: 'string',
          format: 'datetime',
        },
        refreshToken: {
          type: 'string',
          nullable: true,
        },
        refreshTokenExpiresAt: {
          type: 'string',
          format: 'datetime',
          nullable: true,
        },
        scope: {
          type: 'string',
        },
      },
    },
  })
  @ApiOperation({
    operationId: 'oauthToken',
    description: 'A Promise that resolves to the token object returned from Model#saveToken(). In case of an error, the promise rejects with one of the error types derived from OAuthError.',
  })
  @Post('token')
  @HttpCode(HttpStatus.OK)
  public async token(
    @Req() req: Request,
    @Res() res: Response,
  ) {
    return await this.oauth2Service.token(req, res);
  }
}
