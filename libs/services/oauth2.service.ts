import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { OAUTH2_SERVER } from '../constants';
import OAuth2Server from 'oauth2-server';
import { Request, Response } from 'express';

@Injectable()
export class Oauth2Service {
  constructor(@Inject(OAUTH2_SERVER) public readonly oauthServer: OAuth2Server) {
  }

  public async token(req: Request, res: Response) {
    try {
      return await this.oauthServer.token(req, res);
    } catch (e) {
      throw new BadRequestException(e);
    }
  }
}
