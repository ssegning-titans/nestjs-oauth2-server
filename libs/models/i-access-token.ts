import { IClient } from './i-client';
import { IUser } from './i-user';

export interface IAccessToken {
  /**
   * The access token passed to getAccessToken().
   */
  accessToken: string;

  /**
   * The expiry time of the access token.
   */
  accessTokenExpiresAt: Date;

  /**
   * The authorized scope of the access token.
   */
  scope?: string;

  /**
   * The client associated with the access token.
   */
  client: IClient;

  /**
   * The user associated with the access token.
   */
  user: IUser;
}
