export interface IClient {
  id: string;
  redirectUris?: string[];
  grants: string[];
  accessTokenLifetime: number;
  refreshTokenLifetime: number;
}
