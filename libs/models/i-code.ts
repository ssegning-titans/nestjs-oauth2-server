export interface ICode {
  authorizationCode: string;
  expiresAt: Date;

  redirectUri?: string;
  scope?: string;
}
