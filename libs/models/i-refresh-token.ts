import { IClient } from './i-client';
import { IUser } from './i-user';

export interface IRefreshToken {
  /**
   * The refresh token passed to getRefreshToken().
   */
  refreshToken: string;

  /**
   * The expiry time of the refresh token.
   */
  refreshTokenExpiresAt: Date;

  /**
   * The authorized scope of the refresh token.
   */
  scope?: string;

  /**
   * The client associated with the refresh token.
   */
  client: IClient;

  /**
   * The user associated with the refresh token.
   */
  user: IUser;
}
