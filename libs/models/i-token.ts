export interface IToken {
  accessToken: string;
  accessTokenExpiresAt: Date;

  refreshToken?: string;
  refreshTokenExpiresAt?: Date;

  scope?: string;
}
