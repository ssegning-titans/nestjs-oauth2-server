import { IClient } from './i-client';
import { IUser } from './i-user';

export interface IAuthorizationCode {
  code: string;
  expiresAt: Date;

  redirectUri?: string;
  scope?: string;

  client: IClient;
  user: IUser;
}
