import { IAuthCodeService, IClientService, IScopeService, ITokenService, IUserService } from './index';
import { IAccessToken, IAuthorizationCode, IClient, ICode, IRefreshToken, IToken, IUser } from '../models';

export interface Oauth2ServerInterface<CC extends ICode = any,
  C extends IClient = any,
  U extends IUser = any,
  A extends IAuthorizationCode = any,
  AA extends IAccessToken = any,
  R extends IRefreshToken = any,
  T extends IToken = any>
  extends IAuthCodeService<CC, C, U, A>,
    IClientService<C>,
    IScopeService<C, U, AA>,
    ITokenService<AA, R, T, C, U>,
    IUserService<C, U> {
}
