import { IClient } from '../models';

export interface IClientService<C extends IClient> {
  /**
   * Invoked to retrieve a client using a client id or a client id/client secret combination, depending on the grant type.
   *
   * This model function is required for all grant types.
   *
   * ## Invoked during:
   *
   * - authorization_code grant
   * - client_credentials grant
   * - implicit grant
   * - refresh_token grant
   * - password grant
   *
   * @param clientId The client id of the client to retrieve.
   * @param clientSecret The client secret of the client to retrieve. Can be null.
   *
   * @return An Object representing the client and associated data, or a falsy value if no such client could be found.
   */
  getClient(clientId: string, clientSecret: string): Promise<C | false>;
}
