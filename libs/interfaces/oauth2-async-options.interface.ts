import { ModuleMetadata, Type } from '@nestjs/common';
import { OAuth2Options } from './oauth2.options';
import { OAuth2OptionsFactoryInterface } from './oauth2-option-factory.interface';

export interface Oauth2AsyncOptionsInterface extends Pick<ModuleMetadata, 'imports'> {
  useExisting?: Type<OAuth2OptionsFactoryInterface>;
  useClass?: Type<OAuth2OptionsFactoryInterface>;
  useFactory?: (...args: any[]) => Promise<OAuth2Options> | OAuth2Options;
  inject?: any[];
}

