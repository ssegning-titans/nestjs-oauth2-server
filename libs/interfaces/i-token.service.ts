import { IAccessToken, IClient, IRefreshToken, IToken, IUser } from '../models';

export interface ITokenService<A extends IAccessToken, R extends IRefreshToken, T extends IToken, C extends IClient, U extends IUser> {
  /**
   * Invoked to retrieve an existing access token previously saved through Model#saveToken().
   *
   * This model function is required if OAuth2Server#authenticate() is used.
   *
   * ## Invoked during:
   *
   * - request authentication
   *
   * @param accessToken The access token to retrieve.
   *
   * @return An Object representing the access token and associated data.
   */
  getAccessToken(accessToken: string): Promise<A>;

  /**
   * Invoked to retrieve an existing refresh token previously saved through Model#saveToken().
   *
   * This model function is required if the refresh_token grant is used.
   *
   * ## Invoked during:
   *
   * - refresh_token grant
   *
   * @param refreshToken The access token to retrieve.
   *
   * @return An Object representing the refresh token and associated data.
   */
  getRefreshToken?(refreshToken: string): Promise<R>;

  /**
   * Invoked to generate a new access token.
   *
   * This model function is optional. If not implemented, a default handler is used that generates access tokens consisting of 40 characters in the range of a..z0..9.
   *
   * ## Invoked during:
   *
   * - authorization_code grant
   * - client_credentials grant
   * - refresh_token grant
   * - password grant
   *
   * @param client The client the access token is generated for.
   * @param user The user the access token is generated for.
   * @param scope The scopes associated with the access token. Can be null.
   *
   * @return A String to be used as access token.
   */
  generateAccessToken?(client: C, user: U, scope?: string): Promise<string>;

  /**
   * nvoked to generate a new refresh token.
   *
   * This model function is optional. If not implemented, a default handler is used that generates refresh tokens consisting of 40 characters in the range of a..z0..9.
   *
   * ## Invoked during:
   *
   * - authorization_code grant
   * - refresh_token grant
   * - password grant
   *
   * @param client The client the refresh token is generated for.
   * @param user The user the refresh token is generated for.
   * @param scope The scopes associated with the refresh token. Can be null.
   *
   * @return A String to be used as refresh token.
   */
  generateRefreshToken?(client: C, user: U, scope?: string): Promise<string>;

  /**
   * Invoked to save an access token and optionally a refresh token, depending on the grant type.
   *
   * This model function is required for all grant types.
   *
   * ## Invoked during:
   *
   * - authorization_code grant
   * - client_credentials grant
   * - implicit grant
   * - refresh_token grant
   * - password grant
   *
   * @param token The token(s) to be saved.
   * @param client The client associated with the token(s).
   * @param user The user associated with the token(s).
   *
   * @return An Object representing the token(s) and associated data.
   */
  saveToken(token: T, client: C, user: U): Promise<T>;

  /**
   * Invoked to revoke a refresh token.
   *
   * This model function is required if the `refresh_token` grant is used.
   *
   * ## Invoked during:
   *
   * - refresh_token grant
   *
   * @param token is the refresh token object previously obtained through #getRefreshToken().
   *
   * @return `true` if the revocation was successful or `false` if the refresh token could not be found.
   */
  revokeToken(token: R): Promise<boolean>;
}
