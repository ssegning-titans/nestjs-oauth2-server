import { IClient, IUser } from '../models';

export interface IUserService<C extends IClient, U extends IUser> {
  /**
   * Invoked to retrieve a user using a username/password combination.
   *
   * This model function is required if the password grant is used.
   *
   * ## Invoked during:
   *
   * - password grant
   *
   * @param username The username of the user to retrieve.
   * @param password The user’s password.
   *
   * @return An Object representing the user, or a falsy value if no such user could be found. The user object is completely transparent to oauth2-server and is simply used as input to other model functions.
   */
  getUser(username: string, password: string): Promise<U | false>;

  /**
   * Invoked to retrieve the user associated with the specified client.
   *
   * This model function is required if the client_credentials grant is used.
   *
   * ## Invoked during:
   *
   * - client_credentials grant
   *
   * @param client The client to retrieve the associated user for.
   *
   * @return An Object representing the user, or a falsy value if the client does not have an associated user. The user object is completely transparent to oauth2-server and is simply used as input to other model functions.
   */
  getUserFromClient(client: C): Promise<U | false>;
}
