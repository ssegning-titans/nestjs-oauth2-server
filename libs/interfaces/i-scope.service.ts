import { IAccessToken, IClient, IUser } from '../models';

export interface IScopeService<C extends IClient, U extends IUser, A extends IAccessToken> {
  /**
   * Invoked to check if the requested scope is valid for a particular client/user combination.
   *
   * This model function is optional. If not implemented, any scope is accepted.
   *
   * ## Invoked during:
   *
   * - authorization_code grant
   * - client_credentials grant
   * - implicit grant
   * - password grant
   *
   * @param user The associated user.
   * @param client The associated client.
   * @param scope The scopes to validate.
   *
   * @return Validated scopes to be used or a falsy value to reject the requested scopes.
   */
  validateScope(user: U, client: C, scope: string): Promise<string | false>;

  /**
   * Invoked during request authentication to check if the provided access token was authorized the requested scopes.
   *
   * This model function is required if scopes are used with OAuth2Server#authenticate().
   *
   * ## Invoked during:
   *
   * - request authentication
   *
   * @param accessToken The access token to test against
   * @param scope The required scopes.
   *
   * @return `true` if the access token passes, `false` otherwise.
   */
  verifyScope(accessToken: A, scope: string): Promise<boolean>;
}
