import { Oauth2ServerInterface } from './oauth2-server.interface';

export interface OAuth2Options {
  model: Oauth2ServerInterface
}
