import { IAuthorizationCode, IClient, ICode, IUser } from '../models';

export interface IAuthCodeService<CC extends ICode, C extends IClient, U extends IUser, A extends IAuthorizationCode> {
  /**
   * Invoked to generate a new authorization code.
   *
   * This model function is optional. If not implemented, a default handler is used that generates authorization codes consisting of 40 characters in the range of a..z0..9.
   *
   * @param client The client the authorization code is generated for.
   * @param user The user the authorization code is generated for.
   * @param scope The scopes associated with the authorization code. Can be null.
   *
   * @return A String to be used as authorization code.
   */
  generateAuthorizationCode?(client: C, user: U, scope?: string[]): Promise<string>;

  /**
   * Invoked to retrieve an existing authorization code previously saved through Model#saveAuthorizationCode().
   *
   * This model function is required if the authorization_code grant is used.
   *
   * @param authorizationCode The authorization code to retrieve.
   * @return An Object representing the authorization code and associated data.
   */
  getAuthorizationCode?(authorizationCode: string): Promise<A>;

  /**
   * Invoked to save an authorization code.
   *
   * This model function is required if the `authorization_code` grant is used.
   *
   * @param code The code to be saved.
   * @param client The client associated with the authorization code.
   * @param user The user associated with the authorization code.
   *
   * @return An Object representing the authorization code and associated data.
   */
  saveAuthorizationCode(code: CC, client: C, user: U): Promise<A>;

  /**
   * Invoked to revoke an authorization code.
   *
   * This model function is required if the `authorization_code` grant is used.
   *
   * @param code is the authorization code object previously obtained through #getAuthorizationCode().
   * @return `true` if the revocation was successful or `false` if the authorization code could not be found.
   */
  revokeAuthorizationCode(code: A): Promise<boolean>;
}
