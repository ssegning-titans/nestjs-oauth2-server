import { OAuth2Options } from './oauth2.options';

export interface OAuth2OptionsFactoryInterface {
  createOauth2Options(): Promise<OAuth2Options> | OAuth2Options;
}
