import { DynamicModule, Module, Provider } from '@nestjs/common';
import { OAUTH2_SERVER, OAUTH2_SERVER_OPTIONS } from './constants';
import { OAuth2ServerController } from './controllers';
import { Oauth2Service } from './services/oauth2.service';
import OAuth2Server from 'oauth2-server';
import { Oauth2AsyncOptionsInterface, OAuth2Options } from './interfaces';
import { OAuth2OptionsFactoryInterface } from './interfaces/oauth2-option-factory.interface';

@Module({})
export class Oauth2ServerCoreModule {

  public static async forRoot(options: Oauth2AsyncOptionsInterface): Promise<DynamicModule> {
    const providers: Provider[] = this.createAsyncProviders(options);

    return {
      module: Oauth2ServerCoreModule,
      providers: [
        ...providers,
        {
          provide: OAUTH2_SERVER,
          useFactory: async (oAuth2Options: OAuth2Options) => {
            return new OAuth2Server({ model: oAuth2Options.model });
          },
          inject: [OAUTH2_SERVER_OPTIONS],
        },
        Oauth2Service,
      ],
      controllers: [OAuth2ServerController],
      exports: [OAUTH2_SERVER, Oauth2Service],
      global: true,
    };
  }

  private static createAsyncProviders(options: Oauth2AsyncOptionsInterface): Provider[] {
    return [
      this.createAsyncOptionsProvider(options),
    ];
  }

  private static createAsyncOptionsProvider(options: Oauth2AsyncOptionsInterface): Provider {
    if (options.useFactory) {
      return {
        provide: OAUTH2_SERVER_OPTIONS,
        useFactory: options.useFactory,
        inject: options.inject || [],
      };
    }

    return {
      provide: OAUTH2_SERVER_OPTIONS,
      useFactory: async (optionsFactory: OAuth2OptionsFactoryInterface) => {
        return optionsFactory.createOauth2Options();
      },
      inject: [options.useExisting || options.useClass],
    };
  }


}
